# modal-component 
A GSoC iaux project of Internet Archive. And I am gonna have it.
This is a reusable modal component for social sharing and currently under development for "archive.org" website.  
This project uses lit-element for making components.  
Initially there was a doubt in project feasibility so I tried to initialize it by myself and try if the solution is feasible or not. I used these polymers to solve the problem.
 clone the repository https://github.com/maze-runnar/modal-component.git  
 
 cd modal  

> npm install or yarn install

 for starting demo server run the command  

> npm start  

for testing

> npm test

for building  

> npm run build

for running storybook 

> npm run storybook (currently under development)
